import React from 'react';
import Svg, { Path } from 'react-native-svg';

export const PrimaryRightArrowIcon: React.FC<any> = ({ width, height }) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 24 24" fill="none">
      <Path
        d="M9 18l6-6-6-6"
        stroke="#172B4D"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};
// zasagdad magadgui
