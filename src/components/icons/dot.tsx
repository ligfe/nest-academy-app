import * as React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';

export const Dot = () => {
  return (
    <Svg width="4" height="4" viewBox="0 0 4 4" fill="none">
      <Circle cx="2" cy="2" r="2" fill="#303030" />
    </Svg>

  );
};
