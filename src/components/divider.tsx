import React from 'react';
import { Border } from './core';

export const Divider: React.FC<any> = () => {
  return <Border bottomWidth={1} role="primary300" />;
};
