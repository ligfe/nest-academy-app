import React, { useState, createContext, useContext } from 'react';
import { View, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native';
import { Border, Shadow, Text } from './core';
import { Box, Queue, Spacing, Stack } from './layout';
import { DownArrowIcon, Check } from './icons';
import { set } from 'react-native-reanimated';
import { CheckBoxItem, GroupCheckBox } from './group-checkbox';
import { ScrollView } from 'react-native-gesture-handler';

const DropDownContext = createContext({
  visible: false,
  setVisible: (visible: boolean) => {},
  chosenOne: [],
  setChose: Function,
  setChosenOne: (chosenOne: array) => {},
});

export const DropDown = {
  Provider: ({ children }: any) => {
    const [visible, setVisible] = useState<boolean>(false);
    const [chosenOne, setChosenOne] = useState<array>([]);
    return (
      <DropDownContext.Provider
        value={{ visible, setVisible, chosenOne, setChosenOne }}
      >
        {children}
      </DropDownContext.Provider>
    );
  },
  Trigger: ({ children, title }: any) => {
    const { visible, setVisible, chosenOne, setChosenOne } = useContext(
      DropDownContext
    );

    return (
      <TouchableOpacity onPress={() => setVisible(!visible)}>
        <Border
          radius={4}
          role={visible === true ? 'primary500' : 'primary400'}
          lineWidth={visible === true ? 2 : 1}
        >
          <Box
            role={chosenOne.length > 0 ? 'white' : 'primary100'}
            width={'100%'}
          >
            <Spacing pl={4} pr={5} pt={3} pb={3}>
              <Box
                flexDirection={'row'}
                width={'100%'}
                alignItems={'center'}
                justifyContent={'space-between'}
              >
                <Stack size={1} width={'100%'}>
                  <Queue>
                    {chosenOne.length > 0 ? (
                      <Text type={'caption1'} role={'primary400'}>
                        {title}
                      </Text>
                    ) : (
                      <Text width={'auto'} type={'body'} role={'primary400'}>
                        {title}
                      </Text>
                    )}
                    <Text role={'destructive500'} type={'caption1'}>
                      *
                    </Text>
                  </Queue>
                  <Box flexDirection={'row'} width={'100%'}>
                    <Queue size={3}>
                      {chosenOne.length > 0 ? (
                        <Text type={'body'} role={'black'}>
                          {Array.isArray(chosenOne)
                            ? chosenOne.join(',  ')
                            : chosenOne}
                        </Text>
                      ) : (
                        <></>
                      )}
                    </Queue>
                  </Box>
                </Stack>
                <DownArrowIcon width={'12'} height={'6'} />
              </Box>
            </Spacing>
          </Box>
        </Border>
      </TouchableOpacity>
    );
  },
  Content: ({ children, contents, rightIcon = false, setChose }: any) => {
    const { visible, setVisible, chosenOne, setChosenOne } = useContext(
      DropDownContext
    );
    const [index, setIndex] = useState<any>([]);

    const handleDropDownClick = (i: any) => {
      if (!rightIcon) {
        setVisible(!visible);
        setChose(contents[i]);
        setChosenOne(contents[i]);
        return;
      }

      if (index.filter((el: any) => el === i).length) {
        let newIndex = [];
        for (let j = 0; j < index.length; j++) {
          if (index[j] !== i) newIndex.push(index[j]);
        }
        setIndex([...newIndex]);
        setChosenOne([...newIndex.map((el) => contents[el])]);
      } else {
        setChosenOne([...chosenOne, contents[i]]);
        setIndex([...index, i]);
      }
    };
    const renderRightIcon = (i: any) => {
      if (!rightIcon) return;
      if (index.filter((el: any) => el === i).length) {
        return <Check />;
      }
    };
    return (
      <ScrollView>
        <Spacing pt={1}>
          {visible ? (
            React.Children.toArray(children).map((child, i) => {
              return (
                <TouchableOpacity
                  onPress={() => handleDropDownClick(i)}
                  key={i}
                >
                  <Border
                    topWidth={i == 0 ? 1 : 0}
                    bottomWidth={i == children.length - 1 ? 1 : 0}
                    lineWidth={1}
                    role={'primary400'}
                  >
                    <Shadow h={2} radius={4} opacity={0.1}>
                      <Box
                        height={'auto'}
                        width={'100%'}
                        role={'white'}
                        justifyContent={'center'}
                      >
                        <Spacing pl={3} pt={3} pr={3}>
                          <Stack size={3}>
                            <Box
                              width={'100%'}
                              flexDirection={'row'}
                              justifyContent={'space-between'}
                            >
                              {child}
                              {renderRightIcon(i)}
                            </Box>
                            {i !== children.length - 1 ? (
                              <Box
                                height={1}
                                width={'100%'}
                                role={'primary300'}
                              ></Box>
                            ) : (
                              <></>
                            )}
                          </Stack>
                        </Spacing>
                      </Box>
                    </Shadow>
                  </Border>
                </TouchableOpacity>
              );
            })
          ) : (
            <></>
          )}
        </Spacing>
      </ScrollView>
    );
  },
};
