import React, { useState } from 'react';
import { Box, Spacing } from './layout';
import { Border, Text, Button } from './core';
import { ArrowIcon, DownArrowIcon } from './icons';
import { useQuery } from '@apollo/client';

type ExpandType = {
  type?: 'closed' | 'open';
  text?: string;
};

export const ExpandableText: React.FC<ExpandType> = ({
  type = 'closed',
  text,
}) => {
  const [visible, setVisible] = useState(3);
  const [mode, setMode] = useState('Дэлгэрэнгүй');

  return (
    <Box flex={1}>
      <Box>
        <Spacing mb={3}>
          <Text type={'headline'} bold>
            Бүтэн мэдээлэл
          </Text>
        </Spacing>
        <Text
          width={343}
          type={'body'}
          numberOfLines={type === 'closed' ? visible : 100}
        >
          {text}
        </Text>
        <Box flex={1} justifyContent={'center'} alignItems={'center'}>
          <Button
            category={'text'}
            onPress={() => {
              setVisible(visible == 3 ? 100 : 3);
              setMode(mode === 'Дэлгэрэнгүй' ? 'Хураангуй' : 'Дэлгэрэнгүй');
            }}
            width={'100%'}
          >
            <Box flex={1} alignItems={'center'} flexDirection={'row'}>
              <Box>
                <Text bold type={'headline'}>
                  {mode}
                </Text>
              </Box>
              <Spacing p={1} />
              <Box>
                {mode === 'Дэлгэрэнгүй' && <ArrowIcon width={13} height={7} />}
                {mode === 'Хураангуй' && (
                  <DownArrowIcon width={13} height={7} />
                )}
              </Box>
            </Box>
          </Button>
        </Box>
      </Box>
    </Box>
  );
};
