import { fromPairs } from 'lodash';
import React from 'react';
import { Svg, Rect } from 'react-native-svg';

export const RedIndicator: React.FC<any> = () => {
  return (
    <Svg width="14" height="14" viewBox="0 0 14 14" fill="none">
      <Rect x="1" y="1" width="12" height="12" rx="6" fill="#F02848" />
      <Rect
        x="1"
        y="1"
        width="12"
        height="12"
        rx="6"
        stroke="white"
        stroke-width="2"
      />
    </Svg>
  );
};
