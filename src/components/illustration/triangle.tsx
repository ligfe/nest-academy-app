import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

export const Triangle = () => {
  return (
    <Svg width={147} height={77} viewBox="0 0 147 77" fill="none">
      <Path
        opacity={0.5}
        d="M8.587 141.694l14.03-131.588 112.409 68.865-126.44 62.723z"
        stroke="#FF9500"
        strokeWidth={16}
      />
    </Svg>
  );
};
