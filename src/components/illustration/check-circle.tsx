import * as React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';
import { IconType } from '../types';

export const CheckCircleIllustration: React.FC<IconType> = ({
  width = 172,
  height = 172,
}) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 172 172" fill="none">
      <Circle opacity={0.05} cx={86} cy={86} r={86} fill="#4FB83D" />
      <Circle opacity={0.05} cx={86} cy={86} r={70} fill="#4FB83D" />
      <Path
        d="M86 32c-29.777 0-54 24.223-54 54s24.223 54 54 54 54-24.223 54-54-24.223-54-54-54z"
        fill="#4FB83D"
      />
      <Path
        d="M113.369 74.557l-29.25 29.249a4.486 4.486 0 01-3.181 1.319 4.486 4.486 0 01-3.182-1.319L63.132 89.182a4.494 4.494 0 010-6.363 4.494 4.494 0 016.362 0l11.444 11.443 26.069-26.068a4.493 4.493 0 016.362 0 4.496 4.496 0 010 6.363z"
        fill="#FAFAFA"
      />
    </Svg>
  );
};
