import React from 'react';
import { Linking, TouchableOpacity } from 'react-native';
import { Text, Box, Stack } from '../components';
import _ from 'lodash';

export const RichTextParse: React.FC<any> = ({ data, value, nodeType, content }) => {
    if (nodeType === 'document')
        return <Stack size={4}>{_.map(content, (part, index) => <RichTextParse key={index} {...part} />)}</Stack>

    if (nodeType === 'paragraph')
        return <Box width={'100%'} flexDirection={'row'} flexWrap={'wrap'}>{_.map(content, (part, index) => <RichTextParse key={index} {...part} />)}</Box>

    if (nodeType === 'hyperlink')
        return <TouchableOpacity onPress={() => Linking.openURL(data && data.uri)}>
            <Text width={'auto'} role={'accentNest'} underline>{_.map(content, (part, index) => <RichTextParse key={index} {...part} />)}</Text>
        </TouchableOpacity>

    return <Text type={'body'}>{value}</Text>
}