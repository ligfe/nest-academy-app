import React, { useEffect, useState } from 'react';
import { Banner, Border, NestLogo, Shadow, Text } from '../../components';
import { Box, Spacing, Stack } from '../../components/layout';
import { Calendar } from 'react-native-calendars';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import { REQUEST_MEETING_APPOINTMENT } from '../../graphql/queries';
import { useQuery } from '@apollo/client';
import { useCollection } from '../../hooks';

export const ScheduleMeetingCalendarScreen = () => {
  const { data, loading } = useQuery(REQUEST_MEETING_APPOINTMENT);
  const [datesMarked, setDatesMarked] = useState({});
  const [availableTimes, setavailableTimes] = useState({});
  const { collection: meetings, loading: meetingLoading } =
    useCollection('meetingAppointment');

  const untilDay = data?.meetingAppointmentCollection.items[0].thatSpecificDay;
  const availableDays =
    data?.meetingAppointmentCollection.items[0].availableDaysCollection.items;
  const timeTable =
    data?.meetingAppointmentCollection.items[0].availableTimesCollection.items;

  useEffect(() => {
    if (data && meetings) {
      getDays();
    }
  }, [data, meetings]);

  const getDays = async () => {
    let days = {};
    let freeTimes: any = {};
    let totalDays =
      moment(untilDay).diff(moment(), 'days') < 0
        ? 7
        : moment(untilDay).diff(moment(), 'days');

    for (let i = 0; i < totalDays; i++) {
      if (
        availableDays.filter(
          (day: any) => day.number === moment().add(i, 'days').isoWeekday()
        ).length > 0
      ) {
        let times = checkDayIsAvailable(
          moment().add(i, 'days').format('YYYY-MM-DD')
        );

        if (times.length > 0) {
          freeTimes[moment().add(i, 'days').format('YYYY-MM-DD')] = times;

          days = {
            ...days,
            [moment().add(i, 'days').format('YYYY-MM-DD')]: {
              marked: true,
              dotColor: '#4FB83D',
            },
          };
        }
      }
    }
    setavailableTimes(freeTimes);
    setDatesMarked(days);
  };

  useEffect(() => {
    // console.log(availableTimes);
  }, [availableTimes]);
  const checkDayIsAvailable = (day: any) => {
    return meetings
      .filter((meet: any) => {
        return meet.date === day;
      })
      .reduce((a: any, b: any) => {
        return a.filter((time: any) => time.availableTime != b.time);
      }, timeTable);
  };
  if (loading) {
    return <Text>Loading...</Text>;
  }

  const navigation = useNavigation();
  return (
    <Spacing grow={1} ml={4} mr={4} mt={12}>
      <Stack size={4}>
        <Spacing mb={2}>
          <Box width={'100%'} height={129}>
            <Banner type={'info'} title={'Мэдэгдэл'}>
              <Text numberOfLines={3} type={'callout'}>
                Та манай мастеруудтай 30 мин ярилцлага хийснээр элсэлтэнд бүрэн
                хамрагдана.
              </Text>
            </Banner>
          </Box>
        </Spacing>
        <Text
          role={'primary500'}
          bold
          type={'headline'}
          fontFamily={'Montserrat'}
        >
          Та боломжит өдрөө сонгоно уу
        </Text>
        <Shadow w={2}>
          <Border radius={4}>
            <Calendar
              theme={{
                arrowColor: 'black',
              }}
              onDayPress={(pickedDay) => {
                const checkDay = Object.keys(datesMarked).find(
                  (cur) => cur === pickedDay.dateString
                );
                if (checkDay) {
                  navigation.navigate(NavigationRoutes.ScheduleMeetingTime, {
                    pickedDay,
                    availableTimes,
                  });
                }
              }}
              markedDates={datesMarked}
            ></Calendar>
          </Border>
        </Shadow>
      </Stack>
    </Spacing>
  );
};
