import React, { useState, useContext, Children, useEffect } from 'react';
import {
  TouchableOpacity,
  LayoutAnimation,
  Platform,
  UIManager,
} from 'react-native';
import { Banner, Border, Button, Text, PopUp, WarnIconBG, RightArrowIcon } from '../../components';
import { Box, Queue, Spacing, Stack } from '../../components/layout';
import { REQUEST_MEETING_APPOINTMENT } from '../../graphql/queries';
import { useQuery } from '@apollo/client';
import { useDocument } from '../../hooks';
import { AuthContext } from '../../provider/auth';
import moment from 'moment';
import firestore from '@react-native-firebase/firestore';
import { NavigationRoutes } from '../../navigation/navigation-params';
import { useNavigation } from '@react-navigation/native';

const TimeTableButton = (props: any) => {
  const { data, loading } = useQuery(REQUEST_MEETING_APPOINTMENT);
  const { date, time, setPopUp, setSelectedTime } = props;
  const [btnWidth, setBtnWidth] = useState('100%');
  const [visible, setVisible] = useState(0);
  const link = data?.meetingAppointmentCollection.items[0].appointmentLink
  let { user } = useContext(AuthContext);
  let { uid } = user || {};
  const { updateRecord: createMeetingAppointment } = useDocument(
    `meetingAppointment/${uid}`
  );
  const userData: any = useDocument(`users/${uid}`);
  console.log(data?.meetingAppointmentCollection.items[0].appointmentLink);

  const create = async () => {
    setSelectedTime(time.availableTime)
    try {
      await createMeetingAppointment({
        time: time.availableTime,
        phoneNumber: user?.phoneNumber,
        date: date,
        mail: userData.doc.mail,
        link: link
      });
    } catch (e) {
      console.log(e);
    }
    setPopUp(true);
    console.log('DONE');
  };

  return (
    <Queue justifyContent={'space-between'}>
      <Box width={btnWidth} height={42}>
        <Border grow={1} radius={4} lineWidth={1} role={'primary300'}>
          <TouchableOpacity
            style={{ backgroundColor: 'black', width: '100%' }}
            onPress={() => {
              LayoutAnimation.configureNext(LayoutAnimation.Presets.linear),
                setBtnWidth(btnWidth === '100%' ? '47%' : '100%'),
                setVisible(visible === 1 ? 0 : 1);
            }}
          >
            <Box
              width={'100%'}
              height={'100%'}
              role={'primary200'}
              justifyContent={'center'}
              alignItems={'center'}
            >
              <Text bold fontFamily={'Montserrat'} type={'subheading'}>
                {time.availableTime}
              </Text>
            </Box>
          </TouchableOpacity>
        </Border>
      </Box>
      <Box height={42} width={'47%'} opacity={visible}>
        <Button width={'100%'} onPress={create}>
          <Text
            bold
            fontFamily={'Montserrat'}
            type={'subheading'}
            role={'white'}
          >
            Сонгох
          </Text>
        </Button>
      </Box>
    </Queue>
  );
};

export const ScheduleMeetingTimeScreen = (props: any) => {
  const navigation = useNavigation();
  const [popUp, setPopUp] = useState(false);
  const { route } = props;
  const { params } = route || {};
  const { pickedDay, availableTimes } = params;
  const date = pickedDay.dateString;
  const { month, day, dateString } = pickedDay;
  const freeTimes = availableTimes[pickedDay.dateString];
  const [selectedTime, setSelectedTime] = useState('');
  // console.log(availableTimes[pickedDay.dateString]);
  const spellMonth = moment().month(month - 1).format('MMM');
  console.log(freeTimes);
  if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  return (
    <Box flex={1}>
      <Spacing ml={4} mr={4} mt={12} mb={4}>
        <Stack size={4}>
          <Spacing mb={2}>
            <Box width={'100%'} height={129}>
              <Banner type={'info'} title={'Мэдэгдэл'}>
                <Text numberOfLines={3} type={'callout'}>
                  Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!
                </Text>
              </Banner>
            </Box>
          </Spacing>

          <Text
            role={'primary500'}
            bold
            type={'headline'}
            fontFamily={'Montserrat'}
          >
            Та боломжит цагаа сонгоно уу
          </Text>
        </Stack>
      </Spacing>
      <Box
        justifyContent={'center'}
        width={'100%'}
        height={54}
        role={'primary300'}
      >
        <Spacing p={4}>
          <Text fontFamily={'Montserrat'} bold type={'headline'}>
            {spellMonth} {day}
          </Text>
        </Spacing>
      </Box>
      <Box>
        <Spacing pt={4} pr={3.5} pl={3.5}>
          <Stack size={3}>
            {freeTimes.map((time) => (
              <TimeTableButton setSelectedTime={setSelectedTime} time={time} date={date} setPopUp={setPopUp} />
            ))}
          </Stack>
        </Spacing>
      </Box>
      {
      popUp &&
      <PopUp isVisible={popUp} height={369} Close={() => { setPopUp(false) }} >
        <WarnIconBG />
        <Box width={'100%'} alignItems={'center'} justifyContent={'center'} >
          <Text bold type={'title3'} fontFamily={'Montserrat'} >Баталгаажуулах</Text>
          <Spacing mt={2} >
            <Text width={200} textAlign={'center'} type={'body'} role={'black'} >Та {date}-ийн өглөөний {selectedTime} цагт сонгохыг зөвшөөрч байна уу</Text>
          </Spacing>
        </Box>
        <Spacing mt={4} >
          <Button size={'s'} onPress={() => {navigation.navigate('SuccessScreen')}} >
            <Box width={100} flexDirection={'row'} justifyContent={'center'} alignItems={'center'} >
              <Spacing mr={2} >
                <Text bold type={'subheading'} role={'white'} >Дуусгах</Text>
              </Spacing>
            </Box>
          </Button>
        </Spacing>
      </PopUp>
      }
    </Box>
  );
};
