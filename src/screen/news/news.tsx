import React from 'react';
import { FlatList, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { BackgroundImage, Box, Button, Spacing, Stack, Text } from '../../components';
import { RichTextParse } from '../../components/contentful-rich-text-parse';
import { useQuery } from '@apollo/client';
import { REQUEST_NEWS_CARD } from '../../graphql/queries';
import _ from 'lodash'


const exampleImg = require('../../assets/images/image1.png');

export const News = ({ route }: any) => {
  const entryId = route.params.entryId;
  const { data, error } = useQuery(REQUEST_NEWS_CARD, { variables: { entryId } })
  const publishedAt = _.get(data, 'news.sys.publishedAt');
  const paragraph1 = _.get(data, 'news.paragraph.json');
  const assets = _.get(data, 'news.assetsCollection.items');
  const paragraph2 = _.get(data, 'news.paragraph2.json');

  return (
    <ScrollView>
      <Box height={'auto'}>
        <BackgroundImage
          width={'100%'}
          aspectRatio={1}
          resizeMode={'cover'}
          source={{ uri: data?.news?.posterImage?.url }}
        />
        <Spacing mt={3} />
        <Spacing mh={4}>
          <Stack size={4}>
            <Text type={'subheading'} role={'black100'} bold>
              {publishedAt && _.replace(publishedAt.substr(0, 10), '-', ' / ')}
            </Text>
            <Text bold type={'title3'}>
              {data?.news?.title}
            </Text>

            <Box width={'100%'}>
              {paragraph1 && <RichTextParse {...paragraph1} />}
            </Box>
          </Stack>
        </Spacing>
        <FlatList
          style={{ paddingLeft: 16 }}
          data={assets}
          renderItem={({ item }) => <BackgroundImage width={300} aspectRatio={1} source={item} resizeMode={'cover'} />}
          keyExtractor={(item) => item.sys.id}
          horizontal
          ItemSeparatorComponent={() => <Spacing mr={6} />}
          showsHorizontalScrollIndicator={false}
        />
        <Spacing mt={6} mb={10} mh={4}>
          {paragraph2 && <RichTextParse {...paragraph2} />}
        </Spacing>
      </Box>
    </ScrollView>
  );
};
