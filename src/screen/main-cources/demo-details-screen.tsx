import React from 'react';
import { FlatList, SafeAreaView, ScrollView, Dimensions, Image } from 'react-native';
import {
  BackgroundImage,
  Box,
  Comment,
  Spacing,
  Text,
  Button,
} from '../../components';
import { PhotoCard } from './photo-card';
import { useQuery } from '@apollo/client';
import { Request_DemoProjects_Info } from '../../graphql/queries'

export const DemoDetailsScreen: React.FC<any> = ({ route, navigation }) => {
  const {
    posterImage,
    description,
  } = route.params;
  const { width } = Dimensions.get('window');
  const entryId = route.params.sys.id;
  const { data } = useQuery(Request_DemoProjects_Info, { variables: { entryId } });

  return (
    <SafeAreaView>
      {data && <ScrollView style={{ width: width }}>
        <BackgroundImage source={posterImage} width={width} height={375} />
        <Box>
          <Spacing mt={6} mb={4} mh={5}>
            <Text type="body" role="primary500" bold>
              Танилцуулга
            </Text>
          </Spacing>
          <Spacing mh={5}>
            <Text role="black" type="body">
              {description}
            </Text>
          </Spacing>
        </Box>
        <Spacing mt={6}>
          <FlatList
            data={route.params.imagesCollection.items}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(_, index) => index + 'about'}
            renderItem={({ item }) => (
              <Box width={width} height={375} justifyContent='center' alignItems='center'>
                <BackgroundImage source={{ uri: item.url }} height={375} width='100%'/>
              </Box>
            )}
          />
        </Spacing>
        <Spacing mt={10} mb={6} mh={5}>
          <Text role="primary500" type="body" bold>
            Сурагчид
          </Text>
        </Spacing>
        <Spacing ml={4}>
          <FlatList
            data={data.demoProjects.studentsCollection.items}
            horizontal
            showsHorizontalScrollIndicator={false}
            ItemSeparatorComponent={() => <Box width={8} />}
            keyExtractor={(_, index) => index + 'student'}
            renderItem={({ item }) => (
              <PhotoCard
                name={item.name}
                title={item.class}
                source={{ uri: item.image === null ? 'https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png' : item.image.url }}
                width={278}
                height={320}
              />
            )}
          />
        </Spacing>
        <Spacing mt={6} mb={6} ml={5}>
          <Text role="primary500" type="body" bold>
            Сэтгэгдэл
          </Text>
        </Spacing>
        <Spacing mb={15} ml={5}>
          <FlatList
            data={data.demoProjects.commentsCollection.items}
            horizontal
            keyExtractor={(_, index) => index + 'comment'}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }) => (
              <Comment
                name={item.author.name}
                title={item.author.position}
                description={item.body.json.content[0].content[0].value}
                source={{ uri: item.author.image.url }}
                width={311}
              />
            )}
          />
        </Spacing>
      </ScrollView>}
      <Box bottom={30} position={'absolute'} width={'90%'} alignSelf={'center'}>
        <Button
          size="l"
          width={'100%'}
          onPress={() => navigation.navigate(NavigationRoutes.Registration)}
        >
          <Text fontFamily={'Montserrat'} type={'callout'} role={'white'} bold>
            ТУРШИЖ ҮЗЭХ
          </Text>
        </Button>
      </Box>
    </SafeAreaView>
  );
};