import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import {
  Text,
  Box,
  Spacing,
  CloseLineIcon,
  Button,
  Queue,
  Stack,
  Border,
  ExcludeIcon,
} from '../../components';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const UserUndefined: React.FC<ModalType> = ({ navigation }) => {
  const content =
    'Та манай аппликэшинд бүртгүүлснээр цааш үргэлжлүүлэх боломжтой.';

  return (
    <Box flex={1} justifyContent={'center'} alignItems={'center'}>
      <TouchableOpacity
        style={StyleSheet.absoluteFill}
        onPress={() => navigation.goBack()}
      >
        <View
          style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(18,18,18,0.4)' },
          ]}
        />
      </TouchableOpacity>

      <Border radius={8}>
        <Box width={'90%'} role={'primary100'}>
          <Spacing mt={5} mr={5}>
            <Queue justifyContent={'flex-end'}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <CloseLineIcon />
              </TouchableOpacity>
            </Queue>
          </Spacing>
          <Spacing mt={15} mb={15} mr={10} ml={10}>
            <Stack size={6} justifyContent={'center'} alignItems={'center'}>
              <ExcludeIcon />
              <Spacing mb={5} />
              <Text
                type={'title1'}
                bold
                fontFamily={'Montserrat'}
                textAlign={'center'}
              >
                {'Ooppss !!'}
              </Text>
              <Text
                fontFamily={'Montserrat'}
                type={'body'}
                textAlign={'center'}
              >
                {content}
              </Text>
              <Button
                width={200}
                onPress={() => navigation.navigate(NavigationRoutes.SignUp)}
                status="default"
              >
                <Text
                  fontFamily={'Montserrat'}
                  role={'white'}
                  type={'callout'}
                  textAlign={'center'}
                  bold
                >
                  БҮРТГҮҮЛЭХ
                </Text>
              </Button>
              <Button
                onPress={() => navigation.navigate(NavigationRoutes.LogIn)}
                category="text"
                size="s"
              >
                <Text
                  fontFamily={'Montserrat'}
                  role={'primary500'}
                  type={'callout'}
                  textAlign={'center'}
                  underline
                  bold
                >
                  Нэвтрэх
                </Text>
              </Button>
            </Stack>
          </Spacing>
        </Box>
      </Border>
    </Box>
  );
};

type ModalType = {
  route: any;
  navigation: any;
};
