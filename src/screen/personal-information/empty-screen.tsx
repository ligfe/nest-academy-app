import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Box, LoadingCircle } from '../../components';

export const EmptyScreen = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} justifyContent={'center'} alignItems={'center'}>
        <LoadingCircle></LoadingCircle>
      </Box>
    </SafeAreaView>
  );
};
